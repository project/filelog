<?php

namespace Drupal\filelog;

/**
 * An exception thrown when a logfile operation fails.
 */
class FileLogException extends \Exception {}
